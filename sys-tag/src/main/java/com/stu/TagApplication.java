package com.stu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ShiTanchao
 * @description 针对表【t_tag】的数据库操作Service
 * @createDate 2023-10-19 10:52:23
 */
@SpringBootApplication
@MapperScan("com.stu.mapper")
public class TagApplication {
    public static void main(String[] args) {
        SpringApplication.run( TagApplication.class,args);
    }
}