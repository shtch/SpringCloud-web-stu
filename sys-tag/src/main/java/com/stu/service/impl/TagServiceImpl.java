package com.stu.service.impl;

import com.stu.entity.Tag;
import com.stu.mapper.TagMapper;
import com.stu.service.TagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author ShiTanchao
 * @description 针对表【t_tag】的数据库操作Service
 * @createDate 2023-10-19 10:52:23
 */
@Service
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag>
    implements TagService{

}




