package com.stu.service;

import com.stu.entity.Tag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author mengshun
* @description 针对表【t_tag】的数据库操作Service
* @createDate 2023-10-19 09:56:52
*/
public interface TagService extends IService<Tag> {

}
