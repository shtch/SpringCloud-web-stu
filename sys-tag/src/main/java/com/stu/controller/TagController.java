package com.stu.controller;

import com.stu.entity.Tag;
import com.stu.service.TagService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author ShiTanchao
 * @description TODO
 * @date 2023/10/19
 */
@RestController
@RequestMapping("tag")
public class TagController {

    @Resource
    private TagService tagService;

    @Value("${server.port}")
    private String port;

    @GetMapping
    public List<Tag> getList() {
        return tagService.list();
    }

    @DeleteMapping("{id}")
    public Boolean deleteTag(@PathVariable("id") Integer id) {
        return tagService.removeById(id);
    }

    @PostMapping
    public Tag addTag(@RequestBody Tag tag) {
        tag.setCreatedate(new Date());
        tagService.save(tag);
        return tag;
    }

    @GetMapping("{id}")
    public Tag getById(@PathVariable("id") Integer id) {
        return tagService.getById(id);
    }
}
