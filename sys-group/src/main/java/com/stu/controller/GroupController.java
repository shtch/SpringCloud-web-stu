package com.stu.controller;

import com.stu.entity.Group;
import com.stu.service.GroupService;
import com.stu.vo.GroupVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author ShiTanchao
 * @description TODO
 * @date 2023/10/19
 */
@RestController
@RequestMapping("group")
public class GroupController {

    @Resource
    private GroupService groupService;

    @Value("${server.port}")
    private String port;

    @GetMapping
    public List<GroupVO> getList() {
        return groupService.getList();
    }

    @PostMapping
    public Group addGroup(@RequestBody Group group) {
        group.setCreatedate(new Date());
        groupService.save(group);
        return group;

    }

    @GetMapping("{id}")
    public Group  getById(@PathVariable Integer id){
        return groupService.getById(id);
    }

}
