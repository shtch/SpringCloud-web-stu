package com.stu.mapper;

import com.stu.entity.Group;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author ShiTanchao
* @description 针对表【t_group】的数据库操作Mapper
* @createDate 2023-10-21 10:53:39
* @Entity com.stu.entity.Group
*/
public interface GroupMapper extends BaseMapper<Group> {

}




