package com.stu.service;

import com.stu.entity.Group;
import com.baomidou.mybatisplus.extension.service.IService;
import com.stu.vo.GroupVO;

import java.util.List;

/**
* @author ShiTanchao
* @description 针对表【t_group】的数据库操作Service
* @createDate 2023-10-21 10:53:39
*/
public interface GroupService extends IService<Group> {

    List<GroupVO> getList();
}
