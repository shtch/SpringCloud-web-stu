package com.stu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stu.client.ClazzClient;
import com.stu.entity.Clazz;
import com.stu.entity.Group;
import com.stu.mapper.GroupMapper;
import com.stu.service.GroupService;
import com.stu.vo.GroupVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
* @author ShiTanchao
* @description 针对表【t_group】的数据库操作Service实现
* @createDate 2023-10-21 10:53:39
*/
@Service
public class GroupServiceImpl extends ServiceImpl<GroupMapper, Group>
    implements GroupService{

    @Resource
    private GroupMapper groupMapper;

    @Resource
    private ClazzClient clazzClient;

    @Override
    public List<GroupVO> getList() {
        List<Group> groups = groupMapper.selectList(null);
        List<GroupVO> groupVOList = new ArrayList<>();
        for (Group group : groups) {
            GroupVO groupVO = new GroupVO();
            BeanUtils.copyProperties(group,groupVO);
            Integer clazzId = Integer.valueOf(group.getClazzid()) ;

            Clazz clazz = clazzClient.getById(clazzId);
            groupVO.setClazz(clazz);
            groupVOList.add(groupVO);
        }
        return groupVOList;
    }
}




