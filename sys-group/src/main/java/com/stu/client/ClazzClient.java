package com.stu.client;

import com.stu.entity.Clazz;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author ShiTanchao
 * @description TODO
 * @date 2023/10/21
 */
@FeignClient("CLAZZ-SERVICE")
public interface ClazzClient {
    @GetMapping("/clazz/{id}")
    Clazz getById(@PathVariable("id") Integer id);
}
