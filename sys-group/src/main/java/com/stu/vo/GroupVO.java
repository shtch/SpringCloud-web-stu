package com.stu.vo;

import com.stu.entity.Clazz;
import com.stu.entity.Group;
import lombok.Data;

/**
 * @author ShiTanchao
 * @description TODO
 * @date 2023/10/20
 */
@Data
public class GroupVO extends Group {

    private Clazz clazz;
}
