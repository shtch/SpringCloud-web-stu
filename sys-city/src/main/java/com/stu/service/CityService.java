package com.stu.service;

import com.stu.entity.City;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author ShiTanchao
* @description 针对表【t_city】的数据库操作Service
* @createDate 2023-10-19 10:52:23
*/
public interface CityService extends IService<City> {

}
