package com.stu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stu.entity.City;
import com.stu.service.CityService;
import com.stu.mapper.CityMapper;
import org.springframework.stereotype.Service;

/**
* @author ShiTanchao
* @description 针对表【t_city】的数据库操作Service实现
* @createDate 2023-10-19 10:52:23
*/
@Service
public class CityServiceImpl extends ServiceImpl<CityMapper, City>
    implements CityService{

}




