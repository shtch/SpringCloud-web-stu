package com.stu.controller;

import com.stu.entity.City;
import com.stu.service.CityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ShiTanchao
 * @description TODO
 * @date 2023/10/19
 */
@RestController
@RequestMapping("city")
@Slf4j
public class CityController {
    @Autowired
    private CityService cityService;

    @Value("${server.port}")
    private String port;

    @GetMapping
    public List<City> getList() {
        return cityService.list();
    }

    @PostMapping
    public City addCity(@RequestBody City city) {
        boolean save = cityService.save(city);
        log.info("===========================");
        log.info(save?"city添加成功":"city添加失败");
        log.info("===========================");
        return city;
    }

    @GetMapping("{id}")
    public City getById(@PathVariable("id") Integer id) {

        return cityService.getById(id);
    }
}
