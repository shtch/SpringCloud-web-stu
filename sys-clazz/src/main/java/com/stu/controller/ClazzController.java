package com.stu.controller;

import cn.hutool.core.util.IdUtil;
import com.stu.entity.Clazz;
import com.stu.service.ClazzService;
import com.stu.vo.ClazzVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author ShiTanchao
 * @description TODO
 * @date 2023/10/19
 */
@RestController
@RequestMapping("clazz")
public class ClazzController {

    @Resource
    private ClazzService clazzService;

    @Value("${server.port}")
    private String port;

    @GetMapping
    public List<ClazzVO>  getList(){

       return clazzService.getClazzVOList();

    }

    @DeleteMapping("{id}")
    public Boolean deleteClazz(@PathVariable("id") Integer id){
        return clazzService.removeById(id);
    }


    @PostMapping
    public  Clazz  addClazz(MultipartFile file,String name,Integer tagId) throws IOException {
        // 文件上传
        String originalFilename = file.getOriginalFilename();
        String filenameExtension = StringUtils.getFilenameExtension(originalFilename);
        //生成的是不带-的字符串
        String simpleUUID = IdUtil.simpleUUID();
        String lastName = simpleUUID+"."+filenameExtension;

        File imgFile = new File("D://upload/" + lastName);
        file.transferTo(imgFile);
        Clazz clazz = new Clazz();
        clazz.setName(name);
        clazz.setTagid(tagId);
        clazz.setPath(lastName);
        clazzService.save(clazz);

        return clazz;
    }

    @GetMapping("{id}")
    public Clazz getById(@PathVariable("id") Integer id){
       return clazzService.getById(id);
    }
}
