package com.stu.mapper;

import com.stu.entity.Clazz;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author ShiTanchao
 * @description 针对表【t_city】的数据库操作Mapper
 * @createDate 2023-10-19 10:52:23
 * @Entity com.stu.springcloud.entity.City
 */
public interface ClazzMapper extends BaseMapper<Clazz> {

}




