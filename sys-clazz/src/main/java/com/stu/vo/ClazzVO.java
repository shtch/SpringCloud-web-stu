package com.stu.vo;

import com.stu.entity.Clazz;
import com.stu.entity.Tag;
import lombok.Data;

/**
 * @author ShiTanchao
 * @description TODO
 * @date 2023/10/20
 */
@Data
public class ClazzVO extends Clazz {

    private Tag tag;

}
