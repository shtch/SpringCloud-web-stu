package com.stu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stu.client.TagClient;
import com.stu.entity.Clazz;
import com.stu.entity.Tag;
import com.stu.mapper.ClazzMapper;
import com.stu.service.ClazzService;
import com.stu.vo.ClazzVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ShiTanchao
 * @description 针对表【t_tag】的数据库操作Service
 * @createDate 2023-10-19 10:52:23
 */
@Service
public class ClazzServiceImpl extends ServiceImpl<ClazzMapper, Clazz> implements ClazzService{

    @Resource
    private ClazzMapper clazzMapper;


    @Resource
    private TagClient tagClient;

    @Override
    public List<ClazzVO> getClazzVOList() {

        //获取班级列表
        List<Clazz> clazzs = clazzMapper.selectList(null);

        List<ClazzVO> collect = clazzs.stream().map(clazz -> {
            ClazzVO clazzVO = new ClazzVO();
            // 对象的拷贝
            BeanUtils.copyProperties(clazz,clazzVO);
            // tag
            Integer tagid = clazz.getTagid();
            // 发送http请求 tag服务  根据标签id 获取标签对象
            Tag tag = tagClient.getById(tagid);
            clazzVO.setTag(tag);
            return clazzVO;
        }).collect(Collectors.toList());


        return collect;
    }
}




