package com.stu.service;

import com.stu.entity.Clazz;
import com.stu.vo.ClazzVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author ShiTanchao
 * @description 针对表【t_tag】的数据库操作Service
 * @createDate 2023-10-19 10:52:23
 */
public interface ClazzService extends IService<Clazz> {

    List<ClazzVO> getClazzVOList();
}
