package com.stu.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @TableName t_student_tag
 */
@TableName(value = "t_student_tag")
@Data
public class StudentTag implements Serializable {
    private Integer sid;

    private Integer tid;

    private static final long serialVersionUID = 1L;
}