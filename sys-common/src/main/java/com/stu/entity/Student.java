package com.stu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @TableName t_student
 */
@TableName(value = "t_student")
@Data
public class Student implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;

    private Integer age;

    private String qq;

    private String phone;

    private Date bir;

    private String starts;

    private String attr;

    private String mark;

    private Integer clazzid;

    private Integer groupid;

    private Integer cityid;

    private static final long serialVersionUID = 1L;
}