package com.stu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @TableName t_city
 */
@TableName(value = "t_city")
@Data
public class City implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;

    private static final long serialVersionUID = 1L;
}