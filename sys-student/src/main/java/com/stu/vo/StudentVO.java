package com.stu.vo;

import com.stu.entity.*;
import lombok.Data;

import java.util.List;

/**
 * @author ShiTanchao
 * @description TODO
 * @date 2023/10/21
 */
@Data
public class StudentVO extends Student {

    private Clazz clazz;
    private Group group;
    private List<Tag> tags;
    private City city;

    private List<Integer>  tagIds;

}
