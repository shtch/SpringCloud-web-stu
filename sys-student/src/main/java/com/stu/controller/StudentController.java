package com.stu.controller;

import com.stu.service.StudentService;
import com.stu.vo.StudentVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author ShiTanchao
 * @description TODO
 * @date 2023/10/19
 */
@RestController
@RequestMapping("student")
public class StudentController {

    @Resource
    private StudentService studentService;

    @Value("${server.port}")
    private String port;

    @GetMapping
    public List<StudentVO> getList() {
        return studentService.getList();
    }

    @PostMapping
    public StudentVO addStudent(@RequestBody StudentVO studentVO) {
        return studentService.addStudent(studentVO);
    }
}
