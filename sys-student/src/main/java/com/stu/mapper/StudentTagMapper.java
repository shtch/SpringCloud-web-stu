package com.stu.mapper;

import com.stu.entity.StudentTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author ShiTanchao
* @description 针对表【t_student_tag】的数据库操作Mapper
* @createDate 2023-10-21 10:54:03
* @Entity com.stu.entity.StudentTag
*/
public interface StudentTagMapper extends BaseMapper<StudentTag> {

}




