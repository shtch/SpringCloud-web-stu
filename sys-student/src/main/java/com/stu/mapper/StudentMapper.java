package com.stu.mapper;

import com.stu.entity.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author ShiTanchao
* @description 针对表【t_student】的数据库操作Mapper
* @createDate 2023-10-21 10:53:55
* @Entity com.stu.entity.Student
*/
public interface StudentMapper extends BaseMapper<Student> {

}




