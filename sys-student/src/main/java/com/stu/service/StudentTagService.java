package com.stu.service;

import com.stu.entity.StudentTag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author ShiTanchao
* @description 针对表【t_student_tag】的数据库操作Service
* @createDate 2023-10-21 10:54:03
*/
public interface StudentTagService extends IService<StudentTag> {

}
