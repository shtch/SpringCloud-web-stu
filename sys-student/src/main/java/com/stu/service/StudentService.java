package com.stu.service;

import com.stu.entity.Student;
import com.baomidou.mybatisplus.extension.service.IService;
import com.stu.vo.StudentVO;

import java.util.List;

/**
* @author ShiTanchao
* @description 针对表【t_student】的数据库操作Service
* @createDate 2023-10-21 10:53:55
*/
public interface StudentService extends IService<Student> {

    List<StudentVO> getList();

    StudentVO addStudent(StudentVO studentVO);
}
