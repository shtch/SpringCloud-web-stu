package com.stu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stu.entity.StudentTag;
import com.stu.service.StudentTagService;
import com.stu.mapper.StudentTagMapper;
import org.springframework.stereotype.Service;

/**
* @author ShiTanchao
* @description 针对表【t_student_tag】的数据库操作Service实现
* @createDate 2023-10-21 10:54:03
*/
@Service
public class StudentTagServiceImpl extends ServiceImpl<StudentTagMapper, StudentTag>
    implements StudentTagService{

}




