package com.stu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stu.client.*;
import com.stu.entity.*;
import com.stu.mapper.StudentMapper;
import com.stu.mapper.StudentTagMapper;
import com.stu.service.StudentService;
import com.stu.utils.DateUtil;
import com.stu.vo.StudentVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ShiTanchao
 * @description 针对表【t_student】的数据库操作Service实现
 * @createDate 2023-10-21 10:53:55
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student>
        implements StudentService {
    @Resource
    private StudentMapper studentMapper;

    @Resource
    private CityClient cityClient;

    @Resource
    private ClazzClient clazzClient;

    @Resource
    private GroupClient groupClient;

    @Resource
    private TagClient tagClient;

    @Resource
    private StudentTagMapper studentTagMapper;

    @Override
    public List<StudentVO> getList() {

        List<Student> students = studentMapper.selectList(null);

        List<StudentVO> studentVOList = new ArrayList<>();
        for (Student student : students) {
            StudentVO studentVO = new StudentVO();
            BeanUtils.copyProperties(student, studentVO);

            // city
            Integer cityid = student.getCityid();
            City city = cityClient.getById(cityid);
            studentVO.setCity(city);

            // clazz
            Integer clazzid = student.getClazzid();
            Clazz clazz = clazzClient.getById(clazzid);
            studentVO.setClazz(clazz);

            // group
            Integer groupid = student.getGroupid();
            Group group = groupClient.getById(groupid);
            studentVO.setGroup(group);

            // tags
            Integer id = student.getId();
            // 根据学生的id查询学生的标签信息
            LambdaQueryWrapper<StudentTag> lambda = new QueryWrapper<StudentTag>().lambda();
            lambda.eq(StudentTag::getSid, id);
            List<StudentTag> studentTags = studentTagMapper.selectList(lambda);
            List<Integer> tids = studentTags.stream().map(studentTag -> studentTag.getTid()).collect(Collectors.toList());
            List<Tag> tags = tids.stream().map(tid -> {
                Tag tag = tagClient.getById(tid);
                return tag;
            }).collect(Collectors.toList());
            studentVO.setTags(tags);

            studentVOList.add(studentVO);
        }
        return studentVOList;
    }

    @Override
    public StudentVO addStudent(StudentVO studentVO) {

        Student student = new Student();
        BeanUtils.copyProperties(studentVO, student);

        Date bir = student.getBir();
        //根据学生的生日 计算相关属性   年龄  属相  星座
        int age = DateUtil.getAge(bir);
        String attr = DateUtil.getYear(bir);
        //星座
        String star = DateUtil.getConstellation(bir);
        student.setAge(age);
        student.setStarts(star);
        student.setAttr(attr);

        // 将学生信息保存到数据库
        studentMapper.insert(student);
        Integer id = student.getId();
        List<Integer> tagIds = studentVO.getTagIds();
        tagIds.forEach(tid -> {
            StudentTag studentTag = new StudentTag();
            studentTag.setSid(id);
            studentTag.setTid(tid);
            studentTagMapper.insert(studentTag);
        });

        BeanUtils.copyProperties(student, studentVO);

        // city
        Integer cityId = student.getCityid();
        City city = cityClient.getById(cityId);
        studentVO.setCity(city);

        // clazz
        Integer clazzId = student.getClazzid();
        Clazz clazz = clazzClient.getById(clazzId);
        studentVO.setClazz(clazz);

        // group
        Integer groupId = student.getGroupid();
        Group group = groupClient.getById(groupId);
        studentVO.setGroup(group);


        // tags
        List<Tag> tags = tagIds.stream().map(tid -> {
            return tagClient.getById(tid);
        }).collect(Collectors.toList());
        studentVO.setTags(tags);
        return studentVO;
    }
}




