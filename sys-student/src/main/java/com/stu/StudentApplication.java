package com.stu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@MapperScan("com.stu.mapper")
@EnableFeignClients
public class StudentApplication {
    public static void main(String[] args) {
        SpringApplication.run( StudentApplication.class,args);
    }
}
