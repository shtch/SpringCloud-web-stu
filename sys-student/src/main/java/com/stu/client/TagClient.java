package com.stu.client;

import com.stu.entity.Tag;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author ShiTanchao
 * @description TODO
 * @date 2023/10/21
 */
@FeignClient("TAG-SERVICE")
public interface TagClient {

    @GetMapping("/tag/{id}")
    Tag getById(@PathVariable("id") Integer id);
}
