package com.stu.client;

import com.stu.entity.Group;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author ShiTanchao
 * @description TODO
 * @date 2023/10/21
 */
@FeignClient("GROUP-SERVICE")
public interface GroupClient {

    @GetMapping("/group/{id}")
    Group getById(@PathVariable Integer id);
}
