package com.stu.client;

import com.stu.entity.City;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author ShiTanchao
 * @description TODO
 * @date 2023/10/21
 */
@FeignClient("CITY-SERVICE")
public interface CityClient {

    @GetMapping("/city/{id}")
    City getById(@PathVariable("id") Integer id);
}
